import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import withStyles from '@material-ui/core/styles/withStyles';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import Tooltip from '@material-ui/core/Tooltip';
import { openEditShiftDialog } from '../../state/actions';

const styles = {
  row: {
    cursor: 'pointer',
  },
};

class ShiftTable extends Component {
  constructor() {
    super();

    this.state = { rowsPerPage: 5, page: 0 };

    this.handleChangePage = this.handleChangePage.bind(this);
    this.handleChangeRowsPerPage = this.handleChangeRowsPerPage.bind(this);
  }

  handleChangePage(e, page) {
    this.setState({ page });
  }

  handleChangeRowsPerPage(e) {
    e.persist();

    this.setState({ rowsPerPage: e.target.value });
  }

  render() {
    const { rowsPerPage, page } = this.state;
    const { classes, employees, roles, shifts } = this.props;
    return (
      <Fragment>
        <TableBody>
          {shifts
            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
            .map((shift, index) => {
              const {
                break_duration,
                employee_id,
                end_time,
                id,
                role_id,
                start_time,
              } = shift;
              return (
                <TableRow
                  classes={{ root: classes.row }}
                  key={`shift-row-${id}`}
                  onClick={() => this.props.openEditShiftDialog(index, shift)}
                >
                  <TableCell>
                    {`${employees[employee_id].first_name} ${
                      employees[employee_id].last_name
                    }`}
                  </TableCell>
                  <TableCell
                    style={{
                      backgroundColor: roles[role_id].background_colour,
                      color: roles[role_id].text_colour,
                    }}
                  >
                    {roles[role_id].name}
                  </TableCell>
                  <TableCell>
                    <Tooltip title="Click to edit shift">
                      <span>{`${start_time.date}, ${start_time.time}`}</span>
                    </Tooltip>
                  </TableCell>
                  <TableCell>
                    <Tooltip title="Click to edit shift">
                      <span>{`${end_time.date}, ${end_time.time}`}</span>
                    </Tooltip>
                  </TableCell>
                  <TableCell>{break_duration / 60}</TableCell>
                </TableRow>
              );
            })}
        </TableBody>
        <TableFooter>
          <TableRow>
            <TablePagination
              count={shifts.length}
              page={page}
              rowsPerPage={rowsPerPage}
              rowsPerPageOptions={[5, 10, 25]}
              onChangePage={this.handleChangePage}
              onChangeRowsPerPage={this.handleChangeRowsPerPage}
            />
          </TableRow>
        </TableFooter>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  employees: state.employees,
  roles: state.roles,
  shifts: state.shifts,
});

export default connect(
  mapStateToProps,
  { openEditShiftDialog },
)(withStyles(styles)(ShiftTable));
