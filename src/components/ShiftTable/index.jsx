import React from 'react';
import Table from '@material-ui/core/Table';
import Header from './Header';
import Body from './Body';

function ShiftTable() {
  return (
    <Table>
      <Header />
      <Body />
    </Table>
  );
}

export default ShiftTable;
