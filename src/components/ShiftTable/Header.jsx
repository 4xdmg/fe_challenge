import React from 'react';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

const columns = [
  'Employee',
  'Role',
  'Start Time',
  'End Time',
  'Break Length (minutes)',
];

function Header() {
  return (
    <TableHead>
      <TableRow>
        {columns.map(title => (
          <TableCell key={title}>{title}</TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

export default Header;
