import React, { Component } from 'react';
import { connect } from 'react-redux';
import ReactGantt, { GanttRow } from 'react-gantt';

class GanttChart extends Component {
  constructor(props) {
    super(props);

    this.state = {
      leftBound: null,
      rightBound: null,
    };
  }

  componentDidMount() {
    const { shifts } = this.props;

    let leftBound = shifts[0].start_time.dateObj;
    let rightBound = shifts[0].end_time.dateObj;

    shifts.forEach(shift => {
      if (shift.start_time.dateObj < leftBound)
        leftBound = shift.start_time.dateObj;
      if (shift.end_time.dateObj > rightBound)
        rightBound = shift.end_time.dateObj;
    });

    this.setState({
      leftBound,
      rightBound,
    });
  }

  render() {
    const { leftBound, rightBound } = this.state;
    const { roles, shifts } = this.props;

    if (!leftBound || !rightBound) return null;

    return (
      <ReactGantt
        dayFormat="DD MMM H:m"
        hourFormat="HH"
        templates={{
          shiftRoster: {
            title: 'Shift Roster',
            steps: [
              {
                name: 'Shift',
                color: '#000',
              },
            ],
          },
        }}
        leftBound={leftBound}
        rightBound={rightBound}
      >
        {shifts.map(shift => {
          return (
            <GanttRow
              barStyle={{
                color: roles[shift.role_id].background_colour,
                height: '30px',
                marginTop: '5px',
                marginBottom: '5px',
              }}
              key={`gantt-row-${shift.id}`}
              templateName="shiftRoster"
              title={`${shift.id}`}
              steps={[shift.start_time.dateObj, shift.end_time.dateObj]}
            />
          );
        })}
      </ReactGantt>
    );
  }
}

const mapStateToProps = state => ({
  employees: state.employees,
  locale: state.locale,
  roles: state.roles,
  shifts: state.shifts,
});

export default connect(mapStateToProps)(GanttChart);
