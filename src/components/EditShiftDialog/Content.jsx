import React from 'react';
import { connect } from 'react-redux';
import DialogContent from '@material-ui/core/DialogContent';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Input from '@material-ui/core/Input';

import { updateEditedShift } from '../../state/actions';

function Content({ employee, shift, role, ...props }) {
  if (!shift) return null;

  return (
    <DialogContent>
      <Grid alignItems="center" container justify="center" spacing={8}>
        <Grid item xs={12}>
          <Typography variant="title">
            {`${employee.first_name} ${employee.last_name}`}
            {'  '}
            <span
              style={{
                backgroundColor: role.background_colour,
                color: role.text_color,
              }}
            >
              {role.name}
            </span>
          </Typography>
        </Grid>
        <Grid item xs={6}>
          <Typography>Start Time</Typography>
          <Input
            type="time"
            value={shift.start_time.time}
            onChange={e =>
              props.updateEditedShift('start_time', e.target.value)
            }
          />
        </Grid>
        <Grid item xs={6}>
          <Typography>End Time</Typography>
          <Input
            type="time"
            value={shift.end_time.time}
            onChange={e => props.updateEditedShift('end_time', e.target.value)}
          />
        </Grid>
      </Grid>
    </DialogContent>
  );
}

const mapStateToProps = state => {
  const { shift } = state.editedShift;

  return {
    employee: !!shift ? state.employees[shift.employee_id] : false,
    role: !!shift ? state.roles[shift.role_id] : false,
    shift,
  };
};

export default connect(
  mapStateToProps,
  { updateEditedShift },
)(Content);
