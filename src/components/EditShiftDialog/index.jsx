import React from 'react';
import { connect } from 'react-redux';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';

import Content from './Content';
import Actions from './Actions';

function EditShiftDialog({ employee, index, open, shift, role, ...props }) {
  if (!open) return null;

  return (
    <Dialog open={open}>
      <DialogTitle>Edit Shift</DialogTitle>
      <Content />
      <Actions />
    </Dialog>
  );
}

const mapStateToProps = state => ({
  open: !!state.editedShift,
});

export default connect(mapStateToProps)(EditShiftDialog);
