import React from 'react';
import { connect } from 'react-redux';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';

import { closeEditShiftDialog, saveEditedShift } from '../../state/actions';

function EditShiftDialog({ index, shift, ...props }) {
  if (!shift) return null;

  return (
    <DialogActions>
      <Button
        color="primary"
        variant="contained"
        onClick={() => {
          props.saveEditedShift(index, shift);
          props.closeEditShiftDialog();
        }}
      >
        Save
      </Button>
      <Button
        color="secondary"
        variant="contained"
        onClick={props.closeEditShiftDialog}
      >
        Cancel
      </Button>
    </DialogActions>
  );
}

const mapStateToProps = state => ({
  index: state.editedShift.index,
  shift: state.editedShift,
});

export default connect(
  mapStateToProps,
  { closeEditShiftDialog, saveEditedShift },
)(EditShiftDialog);
