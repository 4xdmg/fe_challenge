/**
 * transforms array of object into object with id value as keys
 * @param {array of objects} jsonArray
 */
function transformJsonArrayToObj(jsonArray) {
  return jsonArray.reduce(
    (prev, curr) => ({ ...prev, [curr.id]: { ...curr } }),
    {},
  );
}

export default transformJsonArrayToObj;
