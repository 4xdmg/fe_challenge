/**
 * returns date string, time string and date object
 * @param {string} timeString
 * @param {string} timeZone
 */
function splitDateTime(timeString, timeZone) {
  const dateObj = new Date(timeString);
  const [date, time] = dateObj
    .toLocaleString('en-GB', {
      timeZone,
    })
    .split(', ');

  return { date, dateObj, time };
}

export default splitDateTime;
