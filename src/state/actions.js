import actionTypes from './actionTypes';

export const openEditShiftDialog = (index, shift) => ({
  type: actionTypes.OPEN_EDIT_SHIFT_DIALOG,
  payload: { index, shift },
});

export const closeEditShiftDialog = () => ({
  type: actionTypes.CLOSE_EDIT_SHIFT_DIALOG,
});

export const updateEditedShift = (key, value) => ({
  type: actionTypes.UPDATE_EDITED_SHIFT,
  payload: { key, value },
});

export const saveEditedShift = (index, shift) => ({
  type: actionTypes.SAVE_EDITED_SHIFT,
  payload: { index, shift },
});
