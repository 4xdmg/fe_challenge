import { combineReducers } from 'redux';
import actionTypes from './actionTypes';
import transformJsonArrayToObj from '../helpers/transformJsonArrayToObj';
import splitDateTime from '../helpers/splitDateTime';
import files from '../files';

const { locale, employees, roles, shifts } = files;

// transformation would occur in async middleware in real app
const employeesInitialState = transformJsonArrayToObj(employees);
const rolesInitialState = transformJsonArrayToObj(roles);

// split dates and times to reduce formatting multiple times
const shiftsInitialState = shifts.map(({ start_time, end_time, ...shift }) => ({
  ...shift,
  start_time: { ...splitDateTime(start_time, locale.timezone) },
  end_time: { ...splitDateTime(end_time, locale.timezone) },
}));

function editedShift(state = false, action) {
  switch (action.type) {
    case actionTypes.OPEN_EDIT_SHIFT_DIALOG:
      return action.payload;
    case actionTypes.UPDATE_EDITED_SHIFT:
      const { key, value } = action.payload;

      return {
        ...state,
        shift: { ...state.shift, [key]: { ...state.shift[key], time: value } },
      };
    case actionTypes.CLOSE_EDIT_SHIFT_DIALOG:
      return false;
    default:
      return state;
  }
}

function localeReducer(state = locale, action) {
  switch (action.type) {
    default:
      return state;
  }
}

function employeesReducer(state = employeesInitialState, action) {
  switch (action.type) {
    default:
      return state;
  }
}

function rolesReducer(state = rolesInitialState, action) {
  switch (action.type) {
    default:
      return state;
  }
}

function shiftsReducer(state = shiftsInitialState, action) {
  switch (action.type) {
    case actionTypes.SAVE_EDITED_SHIFT:
      const { index, shift } = action.payload;
      return [...state.slice(0, index), shift, ...state.slice(index + 1)];
    default:
      return state;
  }
}

export default combineReducers({
  employees: employeesReducer,
  editedShift,
  locale: localeReducer,
  roles: rolesReducer,
  shifts: shiftsReducer,
});
