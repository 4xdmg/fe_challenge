import locale from './config.json';
import employees from './employees.json';
import roles from './roles.json';
import shifts from './shifts.json';

export default { locale, employees, roles, shifts };

