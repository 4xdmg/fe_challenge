import React from 'react';
import { connect } from 'react-redux';
import AppBar from '@material-ui/core/AppBar';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Toolbar from '@material-ui/core/Toolbar';

function Nav({ locale }) {
  return (
    <AppBar position="static">
      <Toolbar>
        <Grid alignItems="center" container justify="space-between" spacing={8}>
          <Grid item>
            <Typography variant="title">Rosters</Typography>
          </Grid>
          <Grid item>
            <Typography>{locale.location}</Typography>
          </Grid>
        </Grid>
      </Toolbar>
    </AppBar>
  );
}

const mapStateToProps = state => ({
  locale: state.locale,
});

export default connect(mapStateToProps)(Nav);
