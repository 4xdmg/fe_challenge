import React, { Component } from 'react';
import { Provider } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import CssBaseline from '@material-ui/core/CssBaseline';

import store from '../state/store';
import Nav from './Nav';
import Content from './Content';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Provider store={store}>
          <CssBaseline />
          <Grid container direction="column" className="layout">
            <Grid item>
              <Nav />
            </Grid>
            <Grid item style={{ flexGrow: 1 }}>
              <Content />
            </Grid>
          </Grid>
        </Provider>
      </div>
    );
  }
}

export default App;
