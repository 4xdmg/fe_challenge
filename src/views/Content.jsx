import React, { Component, Fragment } from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import Button from '@material-ui/core/Button';
import ShiftTable from '../components/ShiftTable';
import EditShiftDialog from '../components/EditShiftDialog';
import GanttChart from '../components/GanttChart';

const styles = {
  header: { padding: '10px' },
  wrapper: {
    maxHeight: 'calc(100vh - 100px)',
    maxWidth: '100vw',
    overflow: 'auto',
    padding: '10px',
  },
};

class Content extends Component {
  constructor() {
    super();

    this.state = { table: true };
  }

  render() {
    const { table } = this.state;
    const { classes } = this.props;
    return (
      <div className={classes.wrapper}>
        <div className={classes.header}>
          <Button
            color="primary"
            variant="contained"
            onClick={() =>
              this.setState(prevState => ({ table: !prevState.table }))
            }
          >
            View {table ? 'chart' : 'table'}
          </Button>
        </div>
        {table ? (
          <Fragment>
            <EditShiftDialog />
            <ShiftTable />
          </Fragment>
        ) : (
          <GanttChart />
        )}
      </div>
    );
  }
}

export default withStyles(styles)(Content);
